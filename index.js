/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userProfile(){
		let fullName = prompt('What is your Name?');
		let age = prompt('How Old are you?');
		let location = prompt('Where do you live?');
		console.log('Hello! ' + fullName);
		console.log('You are ' + age + 'years old.');
		console.log('You live in ' + location);
	}
	userProfile();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteBand(){
		console.log('1. Kendrick Lamar');
		console.log('2. Kanye West');
		console.log('3. Drake');
		console.log('4. Jay-Z');
		console.log('5. Dr.Dre');
	}
	favoriteBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favoriteMovies(){
		console.log('1. You Got Mail')
		console.log('Rotten Tomatoes Rating: 69%');
		console.log('2. Prince of Persia: Sand of Time');
		console.log('Rotten Tomatoes Rating: 37%');
		console.log('3. Olympus Has Fallen');
		console.log('Rotten Tomatoes Rating: 50%');
		console.log('4. The Wolf of the Wallstreet');
		console.log('Rotten Tomatoes Rating: 80%');
		console.log('5. The Hangover');
		console.log('Rotten Tomatoes Rating: 79%');
				
	}
	favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	let printFriends = function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);